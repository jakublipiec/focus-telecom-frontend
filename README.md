# Preconditions

**You should install:**

**[GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)**  
**[Node.js and NPM](https://nodejs.org/en/download/)**  

*node v8.x*  
*npm v5.x*

*Install and run the server according to this link:*
https://bitbucket.org/jakublipiec/focus-telecom-backend/src/master/                         
(take a look at README.md file)

---

## How to install?

It's nothing hard. Just follow these steps:

1. Create new foler.
2. In this folder, open **GIT** console.
3. Clone the repository by command: **git clone https://jakublipiec@bitbucket.org/jakublipiec/focus-telecom-frontend.git**
4. Go to the downloaded folder by command: `cd focus-telecom-frontend`
5. Install modules: `npm install`

---

## How to run?

Do the following:

1. Enter in **console**:
	- `npm run serve`
2. Wait until **App running at: - Local: http://localhost:8080/** message will appears in **console**
3. Go to the address: http://localhost:8080

## How to use?

1. Enter correct name and phone number
2. Click **"Dołącz"** button
3. You appeard on the chat page
	- you can send messages to other users (enter message and click **"Wyślij"** button)
	- you can start a conversation (click "Połączenie" button)
4. On a conversation page, enter correct phone number and click "Zadzwoń" button
	- if all is well you should start a conversation (in 26 seconds at most). After the conversation you can go back to the login page.
	- otherwise, you will receive a message that you are unable to establish a connection. You can go back to the login page.
