import io from 'socket.io-client';

class ChatManager {
    constructor() {
        this.socket = io.connect('http://localhost:3000');
    }
    sendMessage(message) {
        this.socket.emit('message', message);
    }
    receiveMessage(callback) {
        this.socket.on('message', callback);
    }
    addUser(user) {
        this.socket.emit('join', user);
    }
}
export default new ChatManager()

