import Vue from 'vue'
import VueRouter from 'vue-router'
import StartConversation from '../views/StartConversation.vue'
import Ringing from '../views/Ringing.vue'
import Connected from '../views/Connected.vue'
import Answered from '../views/Answered.vue'
import SignIn from '../views/SignIn.vue'
import Chat from '../views/Chat.vue'
import Failed from '../views/Failed.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    name: 'signIn',
    component: SignIn
  },
  {
    path: '/start',
    name: 'start',
    component: StartConversation
  },
  {
    path: '/ringing',
    name: 'ringing',
    component: Ringing,
    props: true
  },
  {
    path: '/connected',
    name: 'connected',
    component: Connected
  },
  {
    path: '/answered',
    name: 'answered',
    component: Answered
  },
  {
    path: '/chat',
    name: 'chat',
    component: Chat
  },
  {
    path: '/failed',
    name: 'failed',
    component: Failed
  }
]

const router = new VueRouter({
  routes
})

export default router
